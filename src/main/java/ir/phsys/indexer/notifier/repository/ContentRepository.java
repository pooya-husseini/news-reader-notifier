package ir.phsys.indexer.notifier.repository;

import ir.phsys.indexer.notifier.bean.ContentBean;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/15/17
 *         Time: 5:14 PM
 */
public interface ContentRepository extends SolrCrudRepository<ContentBean, String> {
//    List<ContentBean> findByTextContainsAndStatusEqualsAndIdNotInAndPubDateAfter(Set<String> text, String status, List<String> ids, DateTime publishDate, Pageable pageable);
//    List<ContentBean> findByTextContainsAndStatusEqualsAndPubDateAfter(Set<String> text, String status, DateTime publishDate, Pageable pageable);
    @Query("pubDate_dt:[NOW-1DAY TO NOW] AND status_s:\"success\" AND (?0)")
    List<ContentBean> queryResult(String query);
}
