package ir.phsys.indexer.notifier.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by pooya on 6/16/17.
 */
public interface AccountRepository extends JpaRepository<AccountEntity,Long>{
    AccountEntity findFirstByEmail(String email);
}
