package ir.phsys.indexer.notifier.jpa;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by pooya on 6/16/17.
 */

@Entity
@Table(name = "SendEmail")
public class SentEmailEntity {
    private Long id;
    private AccountEntity account;
    private String newsId;
    private Date timestamp;

    public SentEmailEntity() {
    }

    public SentEmailEntity(AccountEntity account, String newsId) {
        this.newsId = newsId;
        this.account = account;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    @ManyToOne
    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }


    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @PrePersist
    public void init() {
        this.timestamp = new Date();
    }
}
