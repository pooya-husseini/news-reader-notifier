package ir.phsys.indexer.notifier.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by pooya on 6/16/17.
 */
public interface SentEmailRepository extends JpaRepository<SentEmailEntity,Long>{
    List<SentEmailEntity> findByAccount_EmailAndTimestampAfter(String email,Date date);
}
