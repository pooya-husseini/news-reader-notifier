package ir.phsys.indexer.notifier.jpa;

import javax.persistence.*;

/**
 * Created by pooya on 6/16/17.
 */

@Entity
@Table(name = "Account",indexes = @Index(name = "SearchIndex",columnList = "email"))
public class AccountEntity {
    private Long id;
    private String email;

    public AccountEntity() {
    }

    public AccountEntity(String email) {
        this.email = email;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
