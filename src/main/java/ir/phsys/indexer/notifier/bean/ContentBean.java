package ir.phsys.indexer.notifier.bean;


import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.Date;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

@SolrDocument(solrCoreName = "collection1")
public class ContentBean {

    private String id;
    private String title;
    private String text;
    private String content;
    private String subject;
    private String description;
    private String comment;
    private String keywords;
    private String category;
    private String links;
    private String publisher;
    private String url;
    @Field("pubDate_dt")
    private Date pubDate;
    private Boolean archived;
    private String status;
    private Integer updateCount;

    public ContentBean() {
    }

    public ContentBean(String id, String title, String text, String content, String links, String publisher, String url, Date pubDate, Boolean archived, String status) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.links = links;
        this.publisher = publisher;
        this.url = url;
        this.pubDate = pubDate;
        this.archived = archived;
        this.status = status;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    @Field("title_t")
    public void setTitle(String title) {
        this.title = title;
    }

    @Id
    public String getId() {
        return id;
    }

    //    @Field
    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    @Field("persian_text")
    public void setText(String text) {
        this.text = text;
    }

    public String getContent() {
        return content;
    }

    @Field("persian_content")
    public void setContent(String content) {
        this.content = content;
    }

    public String getPublisher() {
        return publisher;
    }

    @Field("author")
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getUrl() {
        return url;
    }

    @Field("url_t")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    //    @Field
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    //    @Field
    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    //    @Field
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getKeywords() {
        return keywords;
    }

    //    @Field
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCategory() {
        return category;
    }

    //    @Field
    public void setCategory(String category) {
        this.category = category;
    }

    public String getLinks() {
        return links;
    }

    @Field("links_t")
    public void setLinks(String links) {
        this.links = links;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }


    public Boolean getArchived() {
        return archived;
    }

    @Field("archived_b")
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public String getStatus() {
        return status;
    }

    @Field("status_s")
    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUpdateCount() {
        return updateCount;
    }

    @Field("updateCount")
    public void setUpdateCount(Integer updateCount) {
        this.updateCount = updateCount;
    }
}
