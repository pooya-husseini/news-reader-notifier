package ir.phsys.indexer.notifier.bean;

import java.util.Set;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/17/17
 *         Time: 6:13 PM
 */

public class UserConfig {
    private String name;
    private String email;
    private Set<String> keywords;
    private Integer size;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    public Integer getSize() {

        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }


}

