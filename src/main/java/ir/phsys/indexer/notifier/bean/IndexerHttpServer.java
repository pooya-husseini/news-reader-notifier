package ir.phsys.indexer.notifier.bean;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.solr.client.solrj.ResponseParser;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import java.io.IOException;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/15/17
 *         Time: 6:26 PM
 */
public class IndexerHttpServer extends HttpSolrClient {
    public IndexerHttpServer(String baseURL) {
        super(baseURL);
    }

    public IndexerHttpServer(String baseURL, HttpClient client) {
        super(baseURL, client);
    }

    public IndexerHttpServer(String baseURL, HttpClient client, ResponseParser parser) {
        super(baseURL, client, parser);
    }

    @Override
    protected HttpRequestBase createMethod(SolrRequest request, String collection) throws IOException, SolrServerException {
//        if("collection1".equalsIgnoreCase(collection)) {
//            return super.createMethod(request, collection);
//        }else{
        request.setMethod(SolrRequest.METHOD.POST);
        return super.createMethod(request, null);
//        }
    }
}
