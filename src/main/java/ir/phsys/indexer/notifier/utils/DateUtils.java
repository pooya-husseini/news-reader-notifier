package ir.phsys.indexer.notifier.utils;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;

import java.util.Date;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/17/17
 *         Time: 3:55 PM
 */
public class DateUtils {
    public static String shamsiNow() {
        return date2Shamsi(new Date());
    }
//
    public static String date2Shamsi(Date date) {
        ULocale locale = new ULocale("fa_IR@calendar=persian");
        Calendar persianCalendar = Calendar.getInstance(locale);
        persianCalendar.setTime(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("E dd MMM YYYY, HH:mm:ss",locale);
        return dateFormat.format(date);
    }
}
