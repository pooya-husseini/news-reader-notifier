package ir.phsys.indexer.notifier.scheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.phsys.indexer.notifier.bean.UserConfig;
import ir.phsys.indexer.notifier.mailer.MailerService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/17/17
 *         Time: 6:56 PM
 */

@Service
public class MailerScheduler {

    @Autowired
    private MailerService mailerService;

    @Scheduled(cron = "0 0/30 * * * *")
    public void schedule() throws IOException, MessagingException {
//        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("");
        File file = new File("./user-config.json");
        if(!file.exists()){
            file.createNewFile();
        }
        InputStream stream = new FileInputStream(file);

        String s = IOUtils.toString(stream, "utf-8");
        stream.close();

        ObjectMapper mapper = new ObjectMapper();

        List<UserConfig> userConfig = mapper.readValue(s, mapper.getTypeFactory().constructCollectionType(List.class, UserConfig.class));
        for (UserConfig config : userConfig) {
            try {
                mailerService.sendMail(config);
            } catch (Exception x) {
                x.printStackTrace();
            }
        }
    }
}

