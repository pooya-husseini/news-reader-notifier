package ir.phsys.indexer.notifier;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import ir.phsys.indexer.notifier.bean.IndexerHttpServer;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.BinaryRequestWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.data.solr.server.SolrClientFactory;
import org.springframework.data.solr.server.support.HttpSolrClientFactory;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;

//import ir.phsys.indexer.notifier.repository.ContentRepository;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/15/17
 *         Time: 5:05 PM
 */

@SpringBootApplication
@EnableEncryptableProperties()
@EnableSolrRepositories(basePackages = "ir.phsys")
@EnableJpaRepositories(basePackages = "ir.phsys")
@EnableScheduling
public class Launcher {

    @Value("${solr.password}")
    private String solrPassword;
    @Value("${solr.username}")
    private String solrUsername;
    @Value("${solr.host}")
    private String solrHost;


    public static void main(String[] args) {
        SpringApplication.run(Launcher.class);
    }






    @Bean
    public SolrClient solrClient() throws IOException {
        PoolingHttpClientConnectionManager cxMgr = new PoolingHttpClientConnectionManager();
        cxMgr.setMaxTotal(100);
        cxMgr.setDefaultMaxPerRoute(20);

        HttpClientBuilder httpclient = HttpClientBuilder.create();
        httpclient.setConnectionManager(cxMgr);
//        httpclient.addInterceptorFirst(new PreemptiveAuthInterceptor());
        BasicCredentialsProvider provider = new BasicCredentialsProvider();
        provider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(solrUsername, solrPassword));

        httpclient.setDefaultCredentialsProvider(provider);
        IndexerHttpServer server = new IndexerHttpServer(solrHost, httpclient.build());
        server.setRequestWriter(new BinaryRequestWriter());
        return server;
    }

    @Bean
    public SolrClientFactory solrClientFactory() throws IOException {
        return new HttpSolrClientFactory(solrClient());
    }

    @Bean
    public SolrTemplate solrTemplate() throws IOException {
        return new SolrTemplate(solrClientFactory());
    }


}
