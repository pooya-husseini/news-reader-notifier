package ir.phsys.indexer.notifier.mailer;

import com.google.common.base.Joiner;
import ir.phsys.indexer.notifier.bean.ContentBean;
import ir.phsys.indexer.notifier.bean.UserConfig;
import ir.phsys.indexer.notifier.jpa.AccountEntity;
import ir.phsys.indexer.notifier.jpa.AccountRepository;
import ir.phsys.indexer.notifier.jpa.SentEmailEntity;
import ir.phsys.indexer.notifier.jpa.SentEmailRepository;
import ir.phsys.indexer.notifier.repository.ContentRepository;
import ir.phsys.indexer.notifier.utils.DateUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * @author : Pooya. h
 *         Email : husseini@caspco.ir
 *         Date: 6/17/17
 *         Time: 6:55 PM
 */
@Service
@Transactional
public class MailerService {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private SentEmailRepository repository;
    @Autowired
    private AccountRepository accountRepository;

    public void sendMail(UserConfig config) throws IOException, MessagingException, SolrServerException {

        StringJoiner joinerAnd = new StringJoiner(" OR ");
        for (String s : config.getKeywords()) {
            joinerAnd.add("persian_text:\"" + s + "\"");
        }
        String query = joinerAnd.toString();
//        String query = "pubDate_dt:[NOW-1DAY TO NOW] AND status_s:'success' AND (" + joinerAnd.toString() + ")";
        Date currentDate = org.apache.commons.lang3.time.DateUtils.truncate(new Date(), Calendar.DATE);


        final AccountEntity[] account = {accountRepository.findFirstByEmail(config.getEmail())};
        if (account[0] == null) {
            account[0] = accountRepository.save(new AccountEntity(config.getEmail()));
        }

//        SolrPageRequest solrPageRequest = new SolrPageRequest(1, config.getSize(), Sort.Direction.DESC, "pubDate_dt");
        List<SentEmailEntity> emails = repository.findByAccount_EmailAndTimestampAfter(config.getEmail(),currentDate);

        List<String> newsIds = emails.stream().map(SentEmailEntity::getNewsId).collect(Collectors.toList());

        if (!newsIds.isEmpty()) {
            query += " AND (id:[* TO *] -id:(" + Joiner.on(" ").skipNulls().join(newsIds) + "))";
        }

//        SolrQuery solrQuery = new SolrQuery(query);
//        solrQuery.set("q", query);
//        solrQuery.setStart(0);
//        solrQuery.setRows(50);
//        QueryResponse response = solrTemplate.getSolrClient().query(solrQuery);
//
//        List<ContentBean> contentBeans = solrTemplate.convertQueryResponseToBeans(response, ContentBean.class);

        List<ContentBean> contentBeans = contentRepository.queryResult(query);
        if (contentBeans.isEmpty()) {
            return;
        }


//        List<Pair<String, String>> collect = count.stream().map(i -> Pair.of(i.getId(), i.getLinks())).collect(Collectors.toList());
//        simpleMailMessage.setTo("husseini@caspco.ir");
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        VelocityContext context = new VelocityContext();
        contentBeans.forEach(i -> repository.save(new SentEmailEntity(account[0], i.getId())));

        context.put("name", config.getName());
        context.put("contents", contentBeans);
        context.put("DateUtils", new DateUtils());

        context.put("keywords", Joiner.on(",").join(config.getKeywords()));


        Template template = ve.getTemplate("mail.vm", "UTF-8");

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper message =
                new MimeMessageHelper(mimeMessage, true, "UTF-8"); // t

        message.setTo(config.getEmail());

        message.setText(writer.toString(), true);
        writer.close();
        message.setSubject("آخرین عناوین خبری");
        mailSender.send(mimeMessage);

    }
}